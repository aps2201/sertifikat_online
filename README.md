# Sistem Sertifikasi Online
Ini adalah work in progress untuk membantu memverifikasi sertifikat dari otoritas sertifikat (lembaga, organisasi, perusahaan) menggunakan web untuk memastikan bahwa sertifikat dikeluarkan oleh otoritas yang bersangkutan.  

download R [di sini](https://www.r-project.org/) atau Rstudio [di sini](https://www.rstudio.com/products/rstudio/download/)

## Struktur
Struktur membutuhkan satu folder sebagai parent -- `/certs/` dalam contoh

* parent folder
       + folder sertifikat
         + png sertifikat **cert.png**
         + index.html

## Penentuan angka
1. angka didapat melalui function `certnum()` berdasarkan sistem pengacakan dari [random.org](http://random.org) di dalam file **certnum.R** yang menghasilkan hexadecimal acak berukuran n byte (default 8) dengan nama variabel (untuk kemudahan tracking assignment angka). 
       + penggunaan: [load file ke dalam R (menggunakan `source(~/certnum.R)`](http://www.dummies.com/how-to/content/how-to-source-a-script-in-r.html)
       + tentukan nama dan besar byte mis., `certnum("johanbudi",10)`
1. buatkan folder menggunakan angka acak tersebut di dalam parent folder sertifikat yang sudah ditentukan di dalam web.
1. sekali lagi, gunakan angka acak dari `certnum()` bukan nama variabel (mis., `/certs/59b8a337bd61b20b1c98`, bukan `/certs/johanbudi`)
1. masih mempertimbangkan penggunaan suffix _.

## Tampilan
1. Untuk mempermudah input sertifikat bisa tambahkan index.html berisi form untuk memasukan kode sertifikat di parent folder.
1. tampilan menggunakan html sederhana yang menampilkan png dari sertifikat yang sudah dikeluarkan serta pesan berhasil.
1. tampilan perlu pengembangan yang lebih baik tetapi sekarang masih difokuskan pada fungsionalitasnya.

## Contoh
* [contoh dari input kode](http://virtue.or.id/certs/)
* berikut [contoh dari hasil verifikasi sertifikat](http://virtue.or.id/certs/14d538d804b96ee7/)
* dapat ditemukan dalam folder `/contoh/` di repositori ini.
* folder `/certs/` merupakan parent folder yang mengandung folder-folder sertifikat.
* tiap folder sertifikat memiliki folder dengan nama ***bilangan acak*** yang didapat melalui **certnum.R** .

NB: akan sangat menyenangkan kalau ada yang bisa membantu membuat otomasi dari sistem ini.
